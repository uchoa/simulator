package br.com.vek.Simulator.app.enumerate;

public enum RateTypeEnum {
	
	DEBIT("Débito"),
	CREDIT("Crédito");

	private String description;

	private RateTypeEnum(String description ) {
		setDescription(description);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
