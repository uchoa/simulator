package br.com.vek.Simulator.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.Simulator.app.model.Concurrent;
import br.com.vek.Simulator.app.repository.ConcurrentRepository;

@RestController
@RequestMapping("/concurrent")
public class ConcurrentRestController {
	
	@Autowired
	private ConcurrentRepository concurrentRepository;

	@GetMapping("/getAllConcurrent")
	public List<Concurrent> getAllConcurrent() {
		return concurrentRepository.findAll();
	}
	
	@PostMapping("/createConcurrent" )
	public Concurrent createConcurrent(@RequestBody Concurrent concurrent) {
		return concurrentRepository.save(concurrent);
		
	}
}
