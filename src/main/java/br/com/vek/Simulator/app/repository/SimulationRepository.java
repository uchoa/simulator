package br.com.vek.Simulator.app.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.vek.Simulator.app.enumerate.SimulationStatusEnum;
import br.com.vek.Simulator.app.model.Simulation;

public interface SimulationRepository extends JpaRepository<Simulation, Long> {
	@Query("SELECT s FROM Simulation s WHERE s.simulationStatus = ?1 ")
	ArrayList<Simulation> findSimulationByStatus(SimulationStatusEnum simulationStatus);
}
