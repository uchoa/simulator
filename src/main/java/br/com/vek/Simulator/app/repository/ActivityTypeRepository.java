package br.com.vek.Simulator.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.Simulator.app.model.ActivityType;

public interface ActivityTypeRepository extends JpaRepository<ActivityType, Long>{

}

