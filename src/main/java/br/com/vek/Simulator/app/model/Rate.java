package br.com.vek.Simulator.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;

import br.com.vek.Simulator.app.enumerate.RateTypeEnum;

@Entity
public class Rate extends EntityModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private RateTypeEnum rateType;

	@Column(nullable = false)
	@JoinColumn(name = "activityType_id")
	private ActivityType activityType;

	@Column(nullable = false)
	private Float rate;

	public RateTypeEnum getRateType() {
		return rateType;
	}

	public void setRateType(RateTypeEnum rateType) {
		this.rateType = rateType;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

}
