package br.com.vek.Simulator.app.enumerate;

public enum SimulationStatusEnum {
	ACCEPT("Aceita"),
	REFUSED("Recusada");

	private String description;

	private SimulationStatusEnum(String description ) {
		setDescription(description);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
