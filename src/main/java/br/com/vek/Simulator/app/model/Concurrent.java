package br.com.vek.Simulator.app.model;

import javax.persistence.Entity;

 
@Entity
public class Concurrent  extends EntityModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
