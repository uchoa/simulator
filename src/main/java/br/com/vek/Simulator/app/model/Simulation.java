package br.com.vek.Simulator.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;

import br.com.vek.Simulator.app.enumerate.SimulationStatusEnum;

@Entity
public class Simulation extends EntityModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	@JoinColumn(name = "concurrent_id")
	private Concurrent concurrent;

	private String numberOfIdentification;

	@Column(nullable = false)
	private String phone;

	private String email;

	@Column(nullable = false)
	@JoinColumn(name = "activityType_id")
	private ActivityType activityType;

	@Column(nullable = false)
	private Float debitRateConcurrent;

	@Column(nullable = false)
	private Float debitDiscount;

	@Column(nullable = false)
	private Float creditRateConcurrent;

	@Column(nullable = false)
	private Float creditDiscount;
	
	@Column(nullable = false)
	private Float debitRateFinal;

	@Column(nullable = false)
	private Float creditRateFinal;

	private Date dateOfAccepted;
	
	@Enumerated(EnumType.STRING)
	private SimulationStatusEnum simulationStatus;

	public Concurrent getConcurrent() {
		return concurrent;
	}

	public void setConcurrent(Concurrent concurrent) {
		this.concurrent = concurrent;
	}

	public String getNumberOfIdentification() {
		return numberOfIdentification;
	}

	public void setNumberOfIdentification(String numberOfIdentification) {
		this.numberOfIdentification = numberOfIdentification;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	public Float getDebitRateConcurrent() {
		return debitRateConcurrent;
	}

	public void setDebitRateConcurrent(Float debitRateConcurrent) {
		this.debitRateConcurrent = debitRateConcurrent;
	}

	public Float getDebitDiscount() {
		return debitDiscount;
	}

	public void setDebitDiscount(Float debitDiscount) {
		this.debitDiscount = debitDiscount;
	}

	public Float getCreditRateConcurrent() {
		return creditRateConcurrent;
	}

	public void setCreditRateConcurrent(Float creditRateConcurrent) {
		this.creditRateConcurrent = creditRateConcurrent;
	}

	public Float getCreditDiscount() {
		return creditDiscount;
	}

	public void setCreditDiscount(Float creditDiscount) {
		this.creditDiscount = creditDiscount;
	}

	public Float getDebitRateFinal() {
		return debitRateFinal;
	}

	public void setDebitRateFinal(Float debitRateFinal) {
		this.debitRateFinal = debitRateFinal;
	}

	public Float getCreditRateFinal() {
		return creditRateFinal;
	}

	public void setCreditRateFinal(Float creditRateFinal) {
		this.creditRateFinal = creditRateFinal;
	}

	public Date getDateOfAccepted() {
		return dateOfAccepted;
	}

	public void setDateOfAccepted(Date dateOfAccepted) {
		this.dateOfAccepted = dateOfAccepted;
	}

	public SimulationStatusEnum getSimulationStatus() {
		return simulationStatus;
	}

	public void setSimulationStatus(SimulationStatusEnum simulationStatus) {
		this.simulationStatus = simulationStatus;
	}

}
