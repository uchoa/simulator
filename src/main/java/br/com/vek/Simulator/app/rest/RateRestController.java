package br.com.vek.Simulator.app.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.Simulator.app.model.Rate;
import br.com.vek.Simulator.app.repository.RateRepository;

@RestController
@RequestMapping("/rate")
public class RateRestController {

	@Autowired
	private RateRepository rateRepository ;
	
	@PostMapping("/createRate" )
	public Rate createRate(@RequestBody Rate rate) {
		return rateRepository.save(rate);
		
	}
}
