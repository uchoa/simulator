package br.com.vek.Simulator.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.Simulator.app.model.ActivityType;
import br.com.vek.Simulator.app.repository.ActivityTypeRepository;;

@RestController
@RequestMapping("/activityType")
public class ActivityTypeRestController {

	@Autowired
	private ActivityTypeRepository activityTypeRepository;

	@GetMapping("/getAllActivityType")
	public List<ActivityType> getAllActivityType() {
		return activityTypeRepository.findAll();
	}
	
	@PostMapping("/createActivityType" )
	public ActivityType createActivityType(@RequestBody ActivityType activityType) {
		return activityTypeRepository.save(activityType);
		
	}
}
