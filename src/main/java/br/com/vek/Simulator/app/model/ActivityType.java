package br.com.vek.Simulator.app.model;

import javax.persistence.Entity;

@Entity
public class ActivityType extends EntityModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String description;

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
