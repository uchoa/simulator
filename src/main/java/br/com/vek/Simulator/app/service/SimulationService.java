package br.com.vek.Simulator.app.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vek.Simulator.app.enumerate.RateTypeEnum;
import br.com.vek.Simulator.app.enumerate.SimulationStatusEnum;
import br.com.vek.Simulator.app.model.ActivityType;
import br.com.vek.Simulator.app.model.Rate;
import br.com.vek.Simulator.app.model.Simulation;
import br.com.vek.Simulator.app.repository.RateRepository;
import br.com.vek.Simulator.app.repository.SimulationRepository;

@Service
public class SimulationService {

	@Autowired
	private SimulationRepository simulationRepository;

	@Autowired
	private RateRepository rateRepository;

	private List<String> feadback;

	private static final Logger logger = LoggerFactory.getLogger(SimulationService.class);

	public List<Simulation> findAll() {
		return simulationRepository.findAll();
	}

	public List<Simulation> findSimul() {
		return simulationRepository.findAll();
	}

	public List<String> createAndvalidateSimulation(Simulation simulation) {
		feadback = new ArrayList<>();
		Float creditRate = calculateRate(simulation.getCreditRateConcurrent(), simulation.getCreditDiscount());
		Float debitRate = calculateRate(simulation.getDebitRateConcurrent(), simulation.getDebitDiscount());
		if (validateSimulation(creditRate, RateTypeEnum.CREDIT, simulation.getActivityType())
				& validateSimulation(debitRate, RateTypeEnum.DEBIT, simulation.getActivityType())) {
			simulation.setCreditRateFinal(creditRate);
			simulation.setDebitRateFinal(debitRate);

			if (saveSimulation(simulation)) {
				feadback.add("Simulação permitida.");
				feadback.add(simulation.getId().toString());
			} else {
				feadback.add("Falha ao salvar a simulação.");
			}

		}

		return feadback;
	}

	public List<String> finishSimulation(Simulation simulation) {
		List<String> feadBack = new ArrayList<>();
		simulation.setDateOfAccepted(new Date());
		if (updateSimulation(simulation)) {
			feadBack.add("Simulação finalizada com sucesso.");
		} else {
			feadBack.add("Falha ao finalizar a simulação.");
		}
		return feadBack;
	}

	public File downloadAcceptedSimulation() {
		ArrayList<Simulation> simulations = simulationRepository.findSimulationByStatus(SimulationStatusEnum.ACCEPT);
		return genareteFile(simulations);

	}

	private boolean updateSimulation(Simulation simulation) {

		Optional<Simulation> simulationFromDB = simulationRepository.findById(simulation.getId());

		if (simulationFromDB != null) {
			simulation.setCreditRateFinal(simulationFromDB.get().getCreditRateFinal());
			simulation.setDebitRateFinal(simulationFromDB.get().getDebitRateFinal());
			try {
				simulationRepository.save(simulation);
				return true;
			} catch (Exception e) {
				logger.error("SimulationService  ---------   Falha ao salvar a simulação");
				return false;
			}
		} else {
			logger.error("SimulationService  ---------   Falha ao buscar a simulação");
			return false;
		}

	}

	private boolean saveSimulation(Simulation simulation) {
		try {
			simulationRepository.save(simulation);
			return true;
		} catch (Exception e) {
			logger.error("SimulationService  ---------   Falha ao salvar a simulação");
			return false;
		}
	}

	private Float calculateRate(Float rate, Float discount) {
		Float rateResult = rate - (rate * (discount / 100));
		return rateResult;
	}

	private boolean validateSimulation(Float rateValue, RateTypeEnum rateType, ActivityType activityType) {
		ArrayList<Rate> rateList = rateRepository.findRatesByRateTypeAndActivityType(rateType, activityType);
		for (Rate rate : rateList) {
			if (rateValue < rate.getRate()) {
				feadback.add("A taxa de " + rateType.getDescription() + " está abaixo do permitido, o valor mínimo é:"
						+ rate.getRate());
				return false;
			}
		}
		return true;

	}

	private File genareteFile(ArrayList<Simulation> simulations) {
		try {
			File tempFile = File.createTempFile("tmp", ".tmp");
			BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));

			StringBuilder sb = new StringBuilder();
			sb.append("Concorrente,");
			sb.append("CNPJ/CPF,");
			sb.append("Telefone,");
			sb.append("E-mail,");
			sb.append("Ramo de atividade,");
			sb.append("Taxa de débito da concorrência,");
			sb.append("Taxa de crédito da concorrência,");
			sb.append("Porcentagem de desconto no débito,");
			sb.append("Porcentagem de desconto no crédito,");
			sb.append("Taxa de débito final aceita,");
			sb.append("Taxa de crédito final aceita,");
			sb.append("Data de aceite");
			sb.append('\n');
			for (Simulation simulation : simulations) {
				sb.append(simulation.getConcurrent().getName() + ",");
				sb.append(simulation.getNumberOfIdentification() + ",");
				sb.append(simulation.getPhone() + ",");
				sb.append(simulation.getEmail() + ",");
				sb.append(simulation.getActivityType().getDescription() + ",");
				sb.append(simulation.getDebitRateConcurrent() + ",");
				sb.append(simulation.getCreditRateConcurrent() + ",");
				sb.append(simulation.getDebitDiscount() + ",");
				sb.append(simulation.getCreditDiscount() + ",");
				sb.append(simulation.getDebitRateFinal() + ",");
				sb.append(simulation.getCreditRateFinal() + ",");
				sb.append(simulation.getDateOfAccepted() );

				sb.append('\n');
			}

			out.write(sb.toString());
			out.close();
			return tempFile;

		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Falha ao gerar o arquivo .CSV");
			return null;
		}

	}
}
