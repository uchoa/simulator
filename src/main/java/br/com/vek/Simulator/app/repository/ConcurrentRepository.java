package br.com.vek.Simulator.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.Simulator.app.model.Concurrent;


public interface  ConcurrentRepository extends JpaRepository<Concurrent, Long>{

}
