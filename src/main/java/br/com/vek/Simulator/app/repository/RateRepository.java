package br.com.vek.Simulator.app.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.vek.Simulator.app.enumerate.RateTypeEnum;
import br.com.vek.Simulator.app.model.ActivityType;
import br.com.vek.Simulator.app.model.Rate;

public interface RateRepository extends JpaRepository<Rate, Long>{

	@Query("SELECT r FROM Rate r WHERE r.rateType = ?1 and r.activityType = ?2")
	ArrayList<Rate> findRatesByRateTypeAndActivityType(RateTypeEnum rateType, ActivityType activityType);
}
