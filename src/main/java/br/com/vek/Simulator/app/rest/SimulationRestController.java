package br.com.vek.Simulator.app.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.Simulator.app.model.Simulation;
import br.com.vek.Simulator.app.service.SimulationService;

@RestController
@RequestMapping("/simulation")
public class SimulationRestController {

	@Autowired
	private SimulationService simulationService;

	@GetMapping("/getAllSimulation")
	public List<Simulation> getSimulation() {
		return simulationService.findAll();
	}

	@PostMapping("/saveSimulation")
	public List<String> createSimulation(@RequestBody Simulation simulation) {
		List<String> feadBack = simulationService.createAndvalidateSimulation(simulation);
		return feadBack;
	}

	@GetMapping("/downloadAcceptedSimulation")
	public ResponseEntity<ByteArrayResource> downloadAcceptedSimulation() throws IOException {
		HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=simulation.csv");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");
		File file = simulationService.downloadAcceptedSimulation();
		Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        
        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
	}

	@PostMapping("/finishSimulation")
	public List<String> finishSimulation(@RequestBody Simulation simulation) {
		List<String> feadBack = simulationService.finishSimulation(simulation);
		return feadBack;
	}
}
